#include <ArduinoHttpClient.h>

#include <RESTClient.h>


// Original
#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>

// For POST service
#include <Ethernet.h>
#include <SPI.h>
#include "RestClient.h"// <RESTClient.h>//"RestClient.h" //<HTTPClient.h>
#include <ArduinoJson.h>

// Set these to run example.
#define FIREBASE_HOST "ionic-fcm-ea491.firebaseio.com"
#define FIREBASE_AUTH "2ihefXSZ8h5peaL98WyqzO2UijxGnP5NlB7Fdgya"
#define WIFI_SSID "Some"
#define WIFI_PASSWORD "puerco1234"

// service
#define IP "fcm.googleapis.com"
#define PORT 80
#define POST_URL "/fcm/send"

// Ultrasonido
#define TRIGGER D1
#define ECHO    D2
#define RATIO_DIV 29.1
#define MAX_DISTANCE 20

// NodeMCU Pin D1 > TRIGGER | Pin D2 > ECHO
WiFiClient wifi;
HttpClient client = HttpClient(wifi, IP, PORT);
int status = WL_IDLE_STATUS;

void setup() {
  Serial.begin (9600);
  
  pinMode(TRIGGER, OUTPUT);
  pinMode(ECHO, INPUT);
  pinMode(BUILTIN_LED, OUTPUT);

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }


  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

}

String response;


void loop() {
  
  long duration, distance;
  digitalWrite(TRIGGER, LOW);  
  delayMicroseconds(2); 
  
  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(10); 
  
  digitalWrite(TRIGGER, LOW);
  duration = pulseIn(ECHO, HIGH);
  distance = (duration/2) / RATIO_DIV;
  
  Serial.print(distance);
  Serial.println(" centimeters");
  delay(1000);

  StaticJsonBuffer<200> jsonBuffer;
  char json[256];
  JsonObject& root = jsonBuffer.createObject();
  root["to"] = "/topics/global";
  JsonObject& notification = jsonBuffer.createObject();
  notification["body"] = "Mensaje 123 asd";
  notification["content_available"] = true;
  notification["priority"] = "high";
  notification["body"] = "Mensaje 123 title";
  root["notification"] = notification;
  JsonObject& data = jsonBuffer.createObject();
  data["body"] = "Mensaje 123 asd";
  data["content_available"] = true;
  data["priority"] = "high";
  data["body"] = "Mensaje 123 title";
  root["data"] = data;

  root.printTo(json, sizeof(json));
  Serial.println(json);

  
  client.beginRequest();
  client.post(POST_URL);
  client.sendHeader("Content-Type", "application/json");
  client.sendHeader("Authorization", "key=AIzaSyCBJatGOBjm8Cv0V1GCp52-7z0Ehejnvbw");
  client.sendHeader("Content-Length", sizeof(json));
  client.beginBody();
  client.print(json);
  client.endRequest();

  // read the status code and body of the response
  int statusCode = client.responseStatusCode();
  String response = client.responseBody();

  Serial.print("Status code: ");
  Serial.println(statusCode);
  Serial.print("Response: ");
  Serial.println(response);

  Serial.println("Wait five seconds");
  delay(5000);
  


  

  //if(distance < MAX_DISTANCE){

  /*
  client.setHeader("Authorization: key=AIzaSyCBJatGOBjm8Cv0V1GCp52-7z0Ehejnvbw");
  client.setHeader("Content-Type: application/json");
  StaticJsonBuffer<200> jsonBuffer;
  char json[256];
  JsonObject& root = jsonBuffer.createObject();
  root["to"] = "/topics/global";
  JsonObject& notification = jsonBuffer.createObject();
  notification["body"] = "Mensaje 123 asd";
  notification["content_available"] = true;
  notification["priority"] = "high";
  notification["body"] = "Mensaje 123 title";
  root["notification"] = notification;
  JsonObject& data = jsonBuffer.createObject();
  data["body"] = "Mensaje 123 asd";
  data["content_available"] = true;
  data["priority"] = "high";
  data["body"] = "Mensaje 123 title";
  root["data"] = data;

  root.printTo(json, sizeof(json));
  Serial.print(json);
  int statusCode = client.post(POST_URL, json, &response);
  Serial.print("Status code from server: ");
  Serial.println(statusCode);
  Serial.print("Response body from server: ");
  Serial.println(response);
  */

  //}
  
}
